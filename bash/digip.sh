#!/bin/sh

# This will return your outer IP address.
# Consider adding allias to your bash profile to quickly call this script
dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | sed 's/"//g'
