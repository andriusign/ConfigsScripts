#!/bin/bash
echo "Generate random password."

while :
do
	read -p "How many symbols?:" a
	if [ $a -le 1 ]
	then
		break
	fi
	echo
    < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-$a};echo;
    echo "----------------------------"

    openssl rand -base64 $a
    echo "----------------------------"

    </dev/urandom tr -dc '0123456789!@#$%,./?\|qwertyuiopQWERTYUIOPasdfghjklASDFGHJKLzxcvbnmZXCVBNM' | head -c$a; echo ""
    echo "----------------------------"

# md5sum example, or use sha256sum 
#    date | md5sum
done
