alias ll='ls -la'
alias ..='cd ..'
alias ...='cd .. & cd ..'
alias bb='cd -'
alias qq='exit'
alias cls='clear'

alias md='mkdir'

alias run_script='sh /home/user/some_script.sh'

alias fix_web_perms='chown -R www-data:www-data . && find . -type f -exec chmod 644 {} \; && find . -type d -exec chmod 755 {} \;'
